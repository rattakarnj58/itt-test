// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyB-RKJ9FNGRs0iG20R3AURjcLH35KYqjbs",
    authDomain: "import-to-thailand.firebaseapp.com",
    databaseURL: "https://import-to-thailand.firebaseio.com",
    projectId: "import-to-thailand",
    storageBucket: "import-to-thailand.appspot.com",
    messagingSenderId: "648261237041",
    appId: "1:648261237041:web:c870ea46cbd1b0ea961810",
    measurementId: "G-7Y9QLD2QS1"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

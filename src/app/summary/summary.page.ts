import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from '@ionic-native/file/ngx';
import { Platform } from '@ionic/angular';
import pdfMaker from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMaker.vfs =  pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-summary',
  templateUrl: './summary.page.html',
  styleUrls: ['./summary.page.scss'],
})
export class SummaryPage implements OnInit {
data;
summary;
new_data;
supplier;
product;
origin_fee;
detination_fee;
import_tax;
pdfObj = null;
origin;
detination;
tax;
vat;
total;
unit;
pcs;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
     private file: File, 
     private plt: Platform, private fileOpener: FileOpener
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(data => {
      this.data = JSON.parse(data.data)
      console.log("newdata ===>", this.data);
      this.supplier = this.data.Supplier
      this.product = this.data.Product
      this.origin_fee = this.data.Origin_fee
      this.detination_fee = this.data.Destination_fee
      this.import_tax = this.data.Import_tax

      
      this.origin = parseInt(this.origin_fee.of) + parseInt(this.origin_fee.thc) +
      parseInt(this.origin_fee.ams) + parseInt(this.origin_fee.bl) + parseInt(this.origin_fee.hl)
      parseInt(this.origin_fee.cfs) + parseInt(this.origin_fee.txl)

      console.log(this.detination_fee);
      
      this.detination = parseInt(this.detination_fee.do) + parseInt(this.detination_fee.hl) 
      + parseInt(this.detination_fee.thc) + parseInt(this.detination_fee.sts) 
      + parseInt(this.detination_fee.fac) + parseInt(this.detination_fee.pcs) + parseInt(this.detination_fee.lo) 
      + parseInt(this.detination_fee.cc) + parseInt(this.detination_fee.dm) + parseInt(this.detination_fee.rt)
      + parseInt(this.detination_fee.tk) + parseInt(this.detination_fee.ul)
      
      this.vat = this.import_tax.Vat
      this.tax = this.import_tax.Tax
      this.total = this.import_tax.Total
      this.unit = this.import_tax.Total
      this.pcs = this.import_tax.Total/this.product.Pcs
      

  })

}

backpage(){
  this.router.navigate(['import-tax']);
  console.log("back");
}

save(){

    this.summary = {
      "Origin_cost" : this.origin,
      "Destination_cost" : this.detination,
      "Tax" : this.tax,
      "Vat" : this.vat,
      "Total" : this.total,
      "Unit" : this.unit
    }

    this.new_data = {
      "Supplier" : this.supplier,
      "Product" : this.product,
      "Origin_fee" : this.origin_fee,
      "Import_tax" : this.import_tax,
      "Summary" : this.summary
    }

    console.log("data added");
    this.createPdf();
    // let navigationExtras: NavigationExtras = {
    //   queryParams: {
    //     data: JSON.stringify(this.new_data)
    //   }
    // };
  }

  createPdf(){
    var doc = {
      content: [
          { text: 'HS Code: ' + this.import_tax.Hs, style: 'subheader'},
          'Exchange Rate : 32.1430 THB',
          { text: 'Supplier', style: 'subheader'},
          this.supplier.Name == undefined ? "" :    { text: 'Name: '+this.supplier.Name, style: 'text'},
          this.supplier.Address == undefined ? "" : { text: 'Address: '+this.supplier.Address, style: 'text'},
          this.supplier.Tel == undefined ? "" :     { text: 'Tel: '+this.supplier.Tel, style: 'text'},
          this.supplier.Contact == undefined ? "" : { text: 'Contact: '+this.supplier.Contact, style: 'text'},
          this.supplier.Notes == undefined ? "" :   { text: 'Note: '+this.supplier.Note, style: 'text'},

          { text: 'Product', style: 'subheader'},
          'Term of the Shipment : CIF',
          'Method of the Shipment : Less Than Container Load (LCL)',
          this.product.Description == undefined ? "" :   { text: 'Description: '+this.product.Description, style: 'text'},
          this.product.Pcs == undefined ? "" :   { text: 'Pcs / Unit: '+this.product.Pcs + this.product.Unit, style: 'text'},
          this.product.Weight_ton == undefined ? "" :   { text: 'Weight: '+this.product.Weight_ton + "  " + 'Ton', style: 'text'},
          this.product.volumn_m3 == undefined ? "" :   { text: 'Volumn: '+this.product.Volumn_m3 + 'CBM', style: 'text'},

          { text: 'Summary', style: 'subheader'},
          this.origin == 0 ? "" :   { text: 'Origin Cost: '+this.origin, style: 'text'},
          this.detination == 0 ? "" :   { text: 'Detination Cost: '+this.detination, style: 'text'},
          this.import_tax.Import_tax == undefined ? "" :   { text: 'Import Tax Rate: '+this.import_tax.Import_tax, style: 'text'},
          this.import_tax.Vat == undefined ? "" :   { text: 'Vat 7%: '+this.import_tax.Vat, style: 'text'},
          this.import_tax.Cif == undefined ? "" :   { text: 'CIF Amount: '+this.import_tax.Cif, style: 'text'},
          this.total == undefined ? "" :   { text: 'Total Amount: '+this.total, style: 'text'},
          this.supplier.Pcs == undefined ? "" :   { text: 'Piece / Unit: '+this.import_tax.Pcs + " " +this.import_tax.Unit , style: 'text'},
          { text: 'Piece Cost: '+this.pcs, style: 'text'},

          // { text: 'Items', style: 'subheader'},
          // {
          //     style: 'itemsTable',
          //     table: {
          //         widths: ['*', 75, 75],
          //         body: [
          //             [ 
          //                 { text: 'Description', style: 'itemsTableHeader' },
          //                 { text: 'Quantity', style: 'itemsTableHeader' },
          //                 { text: 'Price', style: 'itemsTableHeader' },
          //             ]
          //         ]
          //     }
          // },
          // {
          //     style: 'totalsTable',
          //     table: {
          //         widths: ['*', 75, 75],
          //         body: [
          //             [
          //                 '',
          //                 'Subtotal',
          //                 'INVOICE'
          //             ],
          //             [
          //                 '',
          //                 'Shipping',
          //                 'INVOICE'
          //             ],
          //             [
          //                 '',
          //                 'Total',
          //                 'INVOICE'
          //             ]
          //         ]
          //     },
          //     layout: 'noBorders'
          // },
      ],
      styles: {
          header: {
              fontSize: 20,
              bold: true,
              margin: [0, 0, 0, 10],
              alignment: 'right'
          },
          subheader: {
              fontSize: 15,
              bold: true,
              margin: [0, 20, 0, 5]
          },
          text: {
            fontSize: 14,
        },
          itemsTable: {
              margin: [0, 5, 0, 15]
          },
          itemsTableHeader: {
              bold: true,
              fontSize: 13,
              color: 'black'
          },
          totalsTable: {
              bold: true,
              margin: [0, 30, 0, 0]
          }
      },
      defaultStyle: {
      }
  }
    this.pdfObj = pdfMaker.createPdf(doc);
    this.downloadPdf();
  }

  downloadPdf(){
    if(this.plt.is('cordova')){

    }else{
      this.pdfObj.download();
    }
  }

  home(){
    this.router.navigate(['/home'])
  }
}
import { IfStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router, NavigationExtras } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {
  supplier: any;
  product: any;
  description: any;
  length: any;
  pcs: any;
  unit: any;
  width: any;
  height: any;
  gross_weight: any;
  quantity;
  weight_ton: any;
  volumn_m3: any;
  _20ctnr_min: any = "12";
  _40ctnr_min: any = "14";
  _40HQctnr_min: any = "34";
  ctnr:any;
  lcl: boolean = true;
  fcl: boolean = false;
  new_data: any;
  carbrand: any;
  shipment: any;
  _shipment: any;
  production: any;
  term: boolean;
  selectedRadioGroup:any;
  selectedShipment:any;
  // private trueFormControl = new FormControl(false);
  // private falseFormControl = new FormControl(false);

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private alertController: AlertController
  ) {
    this.route.queryParams.subscribe(data => {
      this.supplier = JSON.parse(data.data)
      console.log("supplier ===>", this.supplier);
      console.log("data ==>", data);
    })
   }
  ngOnInit() {
    this._shipment = [
      {
        name: "20' ctnr",
        data: this._20ctnr_min,
        value: "20' ctnr",
        text: "20' ctnr",
      }, {
        name: "40' ctnr",
        data: this._40ctnr_min,
        value: "40' ctnr",
        text: "40' ctnr",
      },{
        name: "40'HQ ctnr",
        data: this._40HQctnr_min,
        value: "40'HQ ctnr",
        text: "40'HQ ctnr",
      }
];
  }

  backpage(){
    this.router.navigate(['supplier']);
    console.log("back");
  }

  checkbox(selected_value){
    console.log(selected_value);
    this.unit = selected_value;
  }

  onChange(){
    this.volumn_m3 = this.quantity*((this.width * this.length * this.height)/1000000);
    this.weight_ton = this.quantity*(this.gross_weight/1000);
  }
  
  async presentAlert(message) {
    const alert = await this.alertController.create({
      // cssClass: 'my-custom-class',
      header: 'แจ้งเตือน',
      // subHeader: message,
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }


  // _term = [
  //   {
  //     name: 'lcl',
  //     value: 'lcl',
  //     text: 'LCL',
  //   }, {
  //     name: 'fcl',
  //     value: 'fcl',
  //     text: 'FCL',
  //   },
  // ];

  // radioGroupChange(event) {
  //   console.log("radioGroupChange",event.detail.value);
  //   this.selectedRadioGroup = event.detail.value;
  //   if(this.selectedRadioGroup == 'fcl'){
  //     this.term == false;
  //     console.log(this.term);
  //     console.log("AAAAAA");
  //   }else{
  //     this.term == true;
  //     console.log(this.term);
  //     console.log("BBBBBB");
  //   }
  // }

  // Shipment(event) {
  //   console.log("Shipment", event.detail.value);
  //   this.selectedShipment = event.detail.value;
  // }

  // radioFocus() {
  //   console.log("radioFocus");
  // }

async save(){
    // if(this.selectedRadioGroup == ""){
    //   this.presentAlert("กรุณาเลิอก Term");
    //   return;
    // }

    // if(this.selectedRadioGroup == "fcl"){
    //   this.shipment = "Full Container Load (FCL)"
    //   if(this.selectedShipment == "20' ctnr"){
    //     this.ctnr = this._20ctnr_min
    //   }else if(this.selectedShipment == "40' ctnr"){
    //     this.ctnr = this._40ctnr_min
    //   }else if(this.selectedShipment == "40'HQ ctnr"){
    //     this.ctnr = this._40HQctnr_min
    //   }else{
    //     this.presentAlert("กรุณาเลิอก Shipment");
    //     return;
    //   }
    // }else{
    //   this.shipment = "Less Than Container Load (LCL)"
    // }
    
    this.product = {
        "Description" : this.description,
        "Pcs" : this.pcs,
        "Unit" : this.unit,
        "Length" : this.length,
        "Width" : this.width,
        "Height" : this.height,
        "Gross_weight" : this.gross_weight,
        "Quantity" : this.quantity,
        "Weight_ton" : this.weight_ton,
        "Volumn_kg" : this.volumn_m3,

     }
  console.log("supplier >>",this.supplier);
  console.log("product >>",this.product);
this.new_data = {
  "Supplier" : this.supplier,
  "Product" : this.product
}

// if(!this.selectedRadioGroup){
//   console.log("fclllll " , this.selectedRadioGroup);
//   this.presentAlert("กรุณาเลือก Method of the Shipment");
//   return;
// }else{

// }
let navigationExtras: NavigationExtras = {
  queryParams: {
    data: JSON.stringify(this.new_data)
  }
};
this.router.navigate(['origin-fee'], navigationExtras);
console.log("new data >>",this.new_data);

}
}

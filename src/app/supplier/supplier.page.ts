import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-supplier',
  templateUrl: './supplier.page.html',
  styleUrls: ['./supplier.page.scss'],
})
export class SupplierPage implements OnInit {
name;
address;
country;
tel;
contact;
note;
supplier;
message;
  constructor(
    private router: Router,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      // cssClass: 'my-custom-class',
      header: 'แจ้งเตือน',
      // subHeader: message,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  backpage(){
    this.router.navigate(['home']);
    console.log("back");
  }

 save(){
   console.log(this.name);
  //  this.name = "a";
  //  this.address = "a";
  //  this.tel = 12344;
  //  this.contact = "qwe";
   if(this.name == undefined){
    this.presentAlert("กรุณากรอกชื่อ");
   }else if(this.address == undefined){
    this.presentAlert("กรุณากรอกที่อยู่");
  }else if(this.tel == undefined){
    this.presentAlert("กรุณากรอกเบอร์โทรศัพท์");
  }else if(this.contact == undefined){
    this.presentAlert("กรุณากรอกชื่อผู้ติดต่อ");
  }

  if(this.name != undefined && this.address != undefined && this.tel != undefined && this.contact != undefined){
    this.supplier = {
      "Name" : this.name,
      "Address" : this.address,
      "Country" : this.country,
      "Tel" : this.tel,
      "Contact" : this.contact,
      "Notes" : this.note
   }

  let navigationExtras: NavigationExtras = {
    queryParams: {
      data: JSON.stringify(this.supplier)
    }
  };
  this.router.navigate(['product'], navigationExtras);
   console.log(this.supplier);
  }
  }

}

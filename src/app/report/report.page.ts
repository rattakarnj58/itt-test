import { Component, OnInit } from '@angular/core';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from '@ionic-native/file/ngx';
import { Platform } from '@ionic/angular';

import pdfMaker from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMaker.vfs =  pdfFonts.pdfMake.vfs;


@Component({
  selector: 'app-report',
  templateUrl: './report.page.html',
  styleUrls: ['./report.page.scss'],
})
export class ReportPage implements OnInit {
  pdfObj = null;

  constructor(private file: File, private plt: Platform, private fileOpener: FileOpener) { }

  ngOnInit() {
  }

  createPdf(){
    var doc = {
      content:[
        {text: 'Header'}
      ]
    }
    this.pdfObj = pdfMaker.createPdf(doc);
  }

  downloadPdf(){
    if(this.plt.is('cordova')){

    }else{
      this.pdfObj.download();
    }
  }

}


import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';


@Component({
  selector: 'app-origin-fee',
  templateUrl: './origin-fee.page.html',
  styleUrls: ['./origin-fee.page.scss'],
})
export class OriginFeePage implements OnInit {
data;
new_data;
supplier;
product;
shipment: any;
of = "";
thc = "";
ams = "";
bl = "";
hl = "";
cfs = "";
txl = "";
origin_fee;
MySelect1:any=[];
moreIndex1:any=1;
new_name:any=[];
new:any=[];
  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(data => {
      this.data = JSON.parse(data.data)
      console.log("newdata ===>", this.data);
      this.supplier = this.data.Supplier
      this.product = this.data.Product
      console.log("==>",this.supplier, this.product);
    })
    this.shipment = this.product.Shipment;
  }

  selectNo1(val1){
    if(val1==1)
    {
     this.MySelect1.push(this.moreIndex1);
     this.moreIndex1++;
    }
    else{
      this.MySelect1.pop(this.moreIndex1);
      this.moreIndex1--;
    }    
  }

  backpage(){
    this.router.navigate(['product']);
    console.log("back");
  }

async save(){
  this.origin_fee = {
    "of" : this.of == "" ? 0 : this.of,
    "thc" : this.thc == "" ? 0 : this.thc,
    "ams" : this.ams == "" ? 0 : this.ams,
    "bl" : this.bl == "" ? 0 : this.bl,
    "hl" : this.hl == "" ? 0 : this.hl,
    "cfs" : this.cfs == "" ? 0 : this.cfs,
    "tlx" : this.txl == "" ? 0 : this.txl,
  }
  this.new_data = {
    "Supplier" : this.supplier,
    "Product" : this.product,
    "Origin_fee" : this.origin_fee
  }
  let navigationExtras: NavigationExtras = {
    queryParams: {
      data: JSON.stringify(this.new_data)
    }
  };
  this.router.navigate(['destination-fee'], navigationExtras);
  console.log("new_data ==>", this.new_data);
    }


}

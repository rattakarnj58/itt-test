import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OriginFeePage } from './origin-fee.page';

describe('OriginFeePage', () => {
  let component: OriginFeePage;
  let fixture: ComponentFixture<OriginFeePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OriginFeePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OriginFeePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OriginFeePageRoutingModule } from './origin-fee-routing.module';

import { OriginFeePage } from './origin-fee.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OriginFeePageRoutingModule
  ],
  declarations: [OriginFeePage]
})
export class OriginFeePageModule {}

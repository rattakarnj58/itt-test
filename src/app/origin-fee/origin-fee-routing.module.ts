import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OriginFeePage } from './origin-fee.page';

const routes: Routes = [
  {
    path: '',
    component: OriginFeePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OriginFeePageRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-destination-fee',
  templateUrl: './destination-fee.page.html',
  styleUrls: ['./destination-fee.page.scss'],
})
export class DestinationFeePage implements OnInit {
  data;
  new_data;
  supplier;
  product;
  origin_fee;
  do = "";
  hl = "";
  thc = "";
  sts = "";
  fac = "";
  pcs = "";
  lo = "";
  cc = "";
  dm = "";
  return = "";
  truck = "";
  unload = "";
  destination_fee;
  MySelect1:any=[];
  moreIndex1:any=1;
  new_name:any=[];
  new:any = [];
  shipment: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(data => {
      this.data = JSON.parse(data.data)
      console.log("newdata ===>", this.data);
      this.supplier = this.data.Supplier
      this.product = this.data.Product
      this.origin_fee = this.data.Origin_fee,
      console.log("==>",this.supplier, this.product, this.origin_fee);
    });
    this.shipment = this.product.Shipment;
  }

  backpage(){
    this.router.navigate(['product']);
    console.log("back");
  }

  selectNo1(val1){
    if(val1==1)
    {
     this.MySelect1.push(this.moreIndex1);
     this.moreIndex1++;
    }
    else{
      this.MySelect1.pop(this.moreIndex1);
      this.moreIndex1--;
    }    
  }

  save(){
    this.destination_fee = {
      "do" : this.do == "" ? 0 : this.do ,
      "thc" : this.thc == "" ? 0 : this.thc,
      "hl" : this.hl == "" ? 0 : this.hl,
      "sts" : this.sts == "" ? 0 : this.sts,
      "fac" : this.fac == "" ? 0 : this.fac,
      "pcs" : this.pcs == "" ? 0 : this.pcs,
      "lo" : this.lo == "" ? 0 : this.lo,
      "cc" : this.cc == "" ? 0 : this.cc,
      "dm" : this.dm == "" ? 0 : this.dm,
      "rt" : this.return == "" ? 0 : this.return,
      "tk" : this.truck == "" ? 0 : this.truck,
      "ul" : this.unload == "" ? 0 : this.unload,
    }
    this.new_data = {
      "Supplier" : this.supplier,
      "Product" : this.product,
      "Origin_fee" : this.origin_fee,
      "Destination_fee" : this.destination_fee
    }
    console.log(">>",this.new_data);
      let navigationExtras: NavigationExtras = {
        queryParams: {
          data: JSON.stringify(this.new_data)
        }
      }
  this.router.navigate(['import-tax'], navigationExtras);
  console.log("new_data ==>", this.new_data);
    }


}

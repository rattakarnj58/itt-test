import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DestinationFeePage } from './destination-fee.page';

const routes: Routes = [
  {
    path: '',
    component: DestinationFeePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DestinationFeePageRoutingModule {}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DestinationFeePage } from './destination-fee.page';

describe('DestinationFeePage', () => {
  let component: DestinationFeePage;
  let fixture: ComponentFixture<DestinationFeePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestinationFeePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DestinationFeePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

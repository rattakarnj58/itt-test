import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DestinationFeePageRoutingModule } from './destination-fee-routing.module';

import { DestinationFeePage } from './destination-fee.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DestinationFeePageRoutingModule
  ],
  declarations: [DestinationFeePage]
})
export class DestinationFeePageModule {}

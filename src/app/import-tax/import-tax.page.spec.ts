import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ImportTaxPage } from './import-tax.page';

describe('ImportTaxPage', () => {
  let component: ImportTaxPage;
  let fixture: ComponentFixture<ImportTaxPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportTaxPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ImportTaxPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

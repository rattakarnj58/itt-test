import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Papa } from 'ngx-papaparse';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-import-tax',
  templateUrl: './import-tax.page.html',
  styleUrls: ['./import-tax.page.scss'],
})
export class ImportTaxPage implements OnInit {
data;
new_data;
supplier
product
origin_fee;
destination_fee;
import;
rate: string = "32.1430 THB";
hs;
tax_rate;
cif;
import_tax;
vat;
total;
csvData: any[] = [];
hearderRow: any[] =[];
  constructor(
    public http: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
    private papa: Papa,
    
  ) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(data => {
      this.data = JSON.parse(data.data)
      console.log("newdata ===>", this.data);
      this.supplier = this.data.Supplier
      this.product = this.data.Product
      this.destination_fee = this.data.Destination_fee
      this.origin_fee = this.data.Origin_fee
      
    })
  }

extactData(res){
  let csvData = res || '';
  this.papa.parse(csvData, {
    complete: parsedData => {
      console.log("1",parsedData);
      console.log("2",parsedData.data.splice(1,1)[0][2]);
      this.hearderRow = parsedData.data.splice(0,1)[0];
      this.csvData = parsedData.data.splice(1,1);
    }
  })

}

  backpage(){
    this.router.navigate(['product']);
    console.log("back");
  }

  call(){
    window.open("tel:1169", '_system');
  }

  openWeb(){ window.open('https://www.customs.go.th/', '_system'); }

  onChange(){
    var c = this.product.Weight_ton*this.product.Pcs;
    
    console.log("cccc", c);
    var i = (c*1)/100;
    var f = 50000
    this.cif = ((c + i + f)*this.tax_rate)/100
    this.vat = (this.cif*7)/100 
    this.total = this.cif+this.vat 
  }

  save(){
    this.import_tax = {
      "Rate" : this.rate,
      "Hs" : this.hs,
      "Tax" : this.tax_rate,
      "Cif" : this.cif,
      "Vat" : this.vat,
      "Total" : this.total
    }

    this.new_data = {
      "Supplier" : this.supplier,
      "Product" : this.product,
      "Origin_fee" : this.origin_fee,
      "Destination_fee" : this.destination_fee,
      "Import_tax" : this.import_tax
    }

    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify(this.new_data)
      }
    };

this.router.navigate(['summary'], navigationExtras);

  }

}

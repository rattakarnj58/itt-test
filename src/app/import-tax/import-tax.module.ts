import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ImportTaxPageRoutingModule } from './import-tax-routing.module';

import { ImportTaxPage } from './import-tax.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImportTaxPageRoutingModule
  ],
  declarations: [ImportTaxPage]
})
export class ImportTaxPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImportTaxPage } from './import-tax.page';

const routes: Routes = [
  {
    path: '',
    component: ImportTaxPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ImportTaxPageRoutingModule {}
